function visib(){
    if(document.getElementById("hiding").style.visibility == "hidden")
	document.getElementById("hiding").style.visibility = "visible";
    else
	document.getElementById("hiding").style.visibility = "hidden";
}


function imgchange(){
    if(document.getElementById("changit").src == "img/patrick.png")
	document.getElementById("changit").src = "img/path.png";
    else
	document.getElementById("changit").src = "img/patrick.png";
}

imgchange();
setInterval(imgchange, 5000);

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
